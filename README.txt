1. unzip this program
If the unzip command isn't already installed on your system, then run:
sudo apt-get install unzip
else run:
unzip comp9900_u_never_sleep_well

2. install python3.6
If the python3 command isn't already installed on your system, then run:
sudo apt-get install python3.6

3. install requirements
3.1 get to path of requirements.txt
cd comp9900_u_never_sleep_well/Work\ Station/
3.2 install requirements
pip3 install -r requirements.txt

# if MongoDB is installed, skip it
4. install MongoDB on linux(root needed)
curl -O https://fastdl.mongodb.org/linux/mongodb-linux-x86_64-3.0.6.tgz
tar -zxvf mongodb-linux-x86_64-3.0.6.tgz
sudo mv mongodb-linux-x86_64-3.0.6/ /usr/local/mongodb
mkdir -p /data/db

5. deploy database
5.1 Enter mongodb
/usr/local/mongodb/bin/mongo

5.2 Create database called comp9900
use comp9900

5.3 create collections
db.createCollection("users")
db.createCollection("searchHistory")
db.createCollection("post")
db.createCollection("reviews")
db.createCollection("admin_account")
db.createCollection("book")
db.createCollection("feedback")

5.4 import collections
Use mongoimport to import collections from Mongodb_database to comp9900 in mongo database.
/usr/local/mongodb/bin/mongoimport -d comp9900 -c users comp9900_u_never_sleep_well/Mongodb_database/users.json
/usr/local/mongodb/bin/mongoimport -d comp9900 -c post comp9900_u_never_sleep_well/Mongodb_database/post.json
/usr/local/mongodb/bin/mongoimport -d comp9900 -c admin_account comp9900_u_never_sleep_well/Mongodb_database/admin_account.json
/usr/local/mongodb/bin/mongoimport -d comp9900 -c book comp9900_u_never_sleep_well/Mongodb_database/book.json

6. run
python3 ~/comp9900_u_never_sleep_well/Work\ Station/run.py

7. view in Google chrome
open Google chrome type http://localhost:5000 in address bar.
(hint: Google Chrome browser would bring you a better browsing experience)
