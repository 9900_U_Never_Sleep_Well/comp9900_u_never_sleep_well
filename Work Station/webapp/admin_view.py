from flask import render_template, session, url_for,request,redirect, make_response, flash
import webapp.perspectiveAPI as wp
from webapp import app
import io

# from matplotlib.figure import Figure
# from matplotlib.backends.backend_agg import FigureCanvasAgg as FigureCanvas

import webapp.commonFunction as common

# do pagination in user management function, 5 users display in one page
pagination = 5

@app.route('/admin_table', methods=['GET', 'POST'])
def admin_table():
    adminname = session['adminname']

    if request.form.get('search') is not None:
        session['target'] = request.form.get('search')
    # print("session['target']",session['target'])

    # delete users
    try:
        username = request.form.get('username')
        wp.deleteUsers(username)
    except:
        pass

    if request.form.get('search') is None and request.form.get('previous_page') is None and request.form.get('next_page') is None:
        session.pop('target', None)
        print("none session")
    else:
        print("session")
    # if session.get('target') is not None:
    if 'target' in session:
        target = session['target']
        print("target: ",target)
        result, num_of_result = wp.search_user_by_email_username(target)
        print("11111")
    else:
        result = wp.adminUserDisplay()
        num_of_result = wp.getNumOfUsers()
        print("22222")

    # pagination
    print(num_of_result)
    num_of_page = num_of_result // pagination + 1 if num_of_result % pagination != 0 else num_of_result // pagination
    print(num_of_page)
    disable = False
    page = 1
    if num_of_page > 1:
        print("need pagination")
        print(request.form.get('next_page'))
        if request.form.get('next_page') is not None:
            print(request.form.get('next_page'))
            page = min(int(request.form.get('next_page')) + 1, num_of_page)
            if page == num_of_page:
                disable = True
            print("next", page)

        elif request.form.get('previous_page') is not None:
                print(request.form.get('previous_page'))
                page = max(1, int(request.form.get('previous_page')) - 1)
                print("previous",page)
    else:
        page = 1
        print("no pagination")

    # change status of users
    try:
        temp = request.form.get('color').split(" ")
        color = temp[0]
        email = temp[1]
        page = int(temp[2])
        wp.updateColorOfUser(email, color)
        print(111111, session['email'])
        if color == 'l' and session['email'] == email:
            session.pop('username', None)
            session.pop('email', None)
            session.pop('userid', None)
            session.pop('curr_user', None)
            print(session['email'])
            print('cccccccccccc')

    except:
        pass

    result = list(result)[(page - 1) * pagination: num_of_result] if page == num_of_page else list(result)[(page - 1) * pagination: page * pagination]
    return render_template("admin_table.html", result=result, adminname=adminname, page=page, disable=disable)


@app.route('/admin_post_table', methods=['GET', 'POST'])
def admin_post_table():
    adminname = session['adminname']
    try:
        username = str(request.form.get('username'))
        wp.deletePosts(username)
    except:
        pass

    num_of_result = wp.get_num_of_posts()
    # pagination
    num_of_page = num_of_result // pagination + 1
    page = 1
    if num_of_page > 1:
        if request.form.get('next_page') is not None:
            page = min(int(request.form.get('next_page')) + 1, num_of_page)
        elif request.form.get('previous_page') is not None:
            page = max(1, int(request.form.get('previous_page')) - 1)
    else:
        page = 1
    result = wp.adminPostDisplay(page)
    return render_template("admin_post_table.html", result=result, adminname=adminname, page=page)


@app.route('/admin_login2', methods=['GET', 'POST'])
def admin_login():
    #     try:
    # if 'adminname' in session:
    #         return redirect(url_for("admin", adminname=session['adminname']))
    # except:
    if request.method == 'POST':
        adminname = request.form.get('adminname')
        session['adminname'] = adminname
        adminpass = request.form.get('adminpass')

        response = wp.adminAccountCheck(adminname,adminpass)
        if response['result'] == 'success':
            # flash(response['message'])
            return redirect(url_for("admin", adminname=adminname))
        elif response['result'] == 'fail':
            flash(response['message'])
        else:
            flash(response['message'])
    return render_template('admin_login2.html')

@app.route('/contact_us_feedback', methods=['GET', 'POST'])
def contact_us_feedback():
    adminname = session['adminname']
    if request.method == 'POST':
        try:
            if request.form.get('feed_delete') != "":
                email, name, phone, message = request.form.get('feed_delete').split(" ")
                wp.delete_feedback(email, name, phone, message)
        except:
            pass

        if request.form.get('search') != "":
            reply_content = request.form.get('search')
            if request.form.get('feed_reply') != "":
                reply_user = request.form.get('feed_reply')
            # print(reply_user, reply_content)
            if request.form.get('username') != "":
                common.sendEmail(reply_user, 'Feedback reply', 'plain', reply_content)
        else:
            # print('Please input someting to reply!')
            pass
    result = wp.get_all_feedback()
    return render_template('contact_us_feedback.html', result=result, adminname=adminname)


@app.route('/admin_logout')
def admin_logout():
   # remove the username from the session if it is there
   session.pop('adminname', None)
   return redirect(url_for('admin_login'))


# @app.route('/deleteUsers', methods=['GET', 'POST'])
# def deleteUsers():
#     if request.method == 'POST':
#         username = request.form.get('username')
#         wp.deleteUsers(username)
#     return redirect('admin_table.html')
#
# @app.route('/deletePost', methods=['GET', 'POST'])
# def deletePosts():
#     if request.method == 'POST':
#         username = request.form.get('username')
#         wp.deleteUsers(username)
#     return redirect('admin_table.html')


@app.route('/admin')
def admin():
    try:
        adminname = session['adminname']
        try:
            price = wp.getAllPrice()
        except:
            price = 0
        numOfUsers = wp.getNumOfUsers()
        numOfPosts = wp.get_num_of_posts()
        transactions = wp.get_num_of_transactions()
        map_visulization = []
        for index in wp.get_all_address_price():
            temp = []
            temp.append(index['street']+index['city']+'NSW, Australia')
            temp.append('$AU'+index['price'])
            map_visulization.append(temp)
        return render_template("admin.html", price=price, adminname=adminname, numOfUsers=numOfUsers,
            numOfPosts=numOfPosts, transactions=transactions,map_visulization=map_visulization)
    except:
        pass



