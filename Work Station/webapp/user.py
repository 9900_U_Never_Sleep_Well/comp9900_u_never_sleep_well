from datetime import datetime
import webapp.commonFunction as common
from flask import render_template, session, url_for, request, redirect, make_response, flash, jsonify, send_file, \
    Blueprint

from werkzeug.security import generate_password_hash, check_password_hash

from webapp import app

from bson import json_util, ObjectId
import json
from hashlib import md5
from flask_pymongo import ObjectId

from tempfile import NamedTemporaryFile
from shutil import copyfileobj
from os import remove
from io import StringIO
from PIL import Image, ImageDraw, ImageFont
import random
import string
import os

loginPage = "login.html"
registerPage = "register.html"
registerPage2 = "register2.html"
registerPage3 = "register3.html"
indexPage = "index.html"
userPage = "user.html"
settingPage = "setting.html"
mypropertyPage = "mypro.html"
myorderPage = "myorder.html"

mongo = common.initMongo()


@app.route('/login', methods=['GET', 'POST'])
def login():
    if request.method == 'POST':
        email = request.form.get("email")
        password = request.form.get("password")
        response = AccountCheck(email, password)

        if response['result'] == 'success':
            curr_user = FindAll(email)
            curr_user_sanitized = json.loads(json_util.dumps(curr_user))
            username = curr_user['username']
            # print('username:', username)
            userid = str(curr_user['_id'])
            # print('userid', userid)
            flash(response['message'])
            session['curr_user'] = curr_user_sanitized
            session['email'] = email
            session['username'] = username
            session['userid'] = userid
            return redirect(url_for("index", username=username))
        elif response['result'] == 'fail':
            flash(response['message'])
        else:
            flash(response['message'])
        # print(email, password)
    return render_template(loginPage)


def FindAll(email):
    res = mongo.db.users.find({'email':email})
    result = list(res)[0]
    # print("findall",result)
    return result


def AccountCheck(email, password):
    result = mongo.db.users.find({'email': email})
    if result.count() > 0:
        return dict(result='success', message='Your password is not correct!') \
            if check_password_hash(list(result)[0]['password'], password) \
            else dict(result='fail', message='Your password is not correct!')
    else:
        return dict(result='error', message='Email does not exist!')

    try:
        list_result = list(result)[0]
        if result.count() > 0 and list_result['status'] == 'f':
            return dict(result='success', message='login successfully!') \
                if check_password_hash(list_result['password'],password) \
                else dict(result='fail', message='Your password is not correct!')
        else:
            return dict(result='error', message='Email does not exist or you are banned from administration!')
    except:
        return dict(result='error', message='Email does not exist or you are banned from administration!')




def CheckValidEmail(email):
    result = mongo.db.users.find({'email':email})
    if result.count() > 0:
        return dict(result='error', message='Email already exists, please login')
    else:
        return dict(result='success', message='Email is valid!')


@app.route('/register', methods=['GET', 'POST'])
def register():
    # print("-----")
    if request.method == 'POST':

        email = request.values.get("email")
        username = request.values.get("username")
        password = request.values.get("password")
        password_hash = generate_password_hash(password)
        createtime = datetime.now()
        code = request.values.get("code")
        response = CheckValidEmail(email)

        if code != session["real_verification_code"]:
            code_vaild = 0
            flash('code is not correct.')
        else:
            code_vaild = 1
        if response['result'] == 'error':
            flash(response['message'])
        # print(email)
        if response['result'] == 'success' and username is not None and code_vaild == 1:
            usericon = avatar(email, 128)
            # print('usericon', usericon)
            user = {
                "email": email,
                "username": username,
                "password": password_hash,
                "createtime": createtime,
                "confirmed": False,
                "comfirmedtime": None,
                "status": 'f',
                "usericon": usericon,
                "reviews_id": [],
                "phone": "",
                "description": "",
                "gender": "",
                "birth_date": "",
                "booked_id": [],
                "toxic_review": 0
            }

            mongo.db.users.insert_one(user)
            session['email']=email
            token = generate_confirmation_token(email)
            confirm_url = url_for('confirm_email', token=token, _external=True)
            html = render_template('activate.html', confirm_url=confirm_url)
            subject = "Please confirm your email"
            msg_typ = "html"
            common.sendEmail(email, subject, msg_typ, html)
            # login_user(user)
            # print("send email ok")
            # flash('A confirmation email has been sent via email.', 'success')
            # print(user)
            return redirect(url_for('register2'))

    return render_template(registerPage)


@app.route('/register2', methods=['GET', 'POST'])
def register2():
    return render_template(registerPage2)


@app.route('/register3', methods=['GET', 'POST'])
def register3():
    session.pop('email', None)
    return render_template(registerPage3)


# give new user a usericon
def avatar(email, size):
    digest = md5(email.lower().encode('utf-8')).hexdigest()
    return 'https://www.gravatar.com/avatar/{}?d=identicon&s={}'.format(
        digest, size)



@app.route('/logout')
def logout():
    # remove the username from the session if it's there
    session.pop('username', None)
    session.pop('email', None)
    session.pop('userid', None)
    session.pop('curr_user', None)
    return redirect('/')


###########################################user profile###################################
@app.route('/user', methods=['GET', 'POST'])
def userprofile():
    if not session.get('username'):
        return redirect(url_for('login'))
    else:
        username = session['username']
        usericon = session['curr_user']['usericon']
        email = session['email']
        userid = session['userid']
    if request.method == 'POST':
        #if update usericon
        if request.files:
            userimage = request.files['usericon']
            filename = userimage.filename
            basepath = os.getcwd() + '/webapp/static/avatar/'
            # print('basepath: ', basepath)
            allowed_extensions = ['png', 'jpg', 'jpeg', 'gif', 'PNG', 'JPG', 'JPEG', 'GIF']
            flag = '.' in filename and filename.rsplit('.', 1)[1] in allowed_extensions
            if not flag:
                return redirect(url_for('user.html'))
            userimage.save('{}{}_{}'.format(basepath, userid, filename))
            mongo.db.users.update({'email': email},
                                  {"$set": {"usericon": '/static/avatar/{}_{}'.format(userid, filename)}})
            session['curr_user']['usericon'] = '/static/avatar/{}_{}'.format(userid, filename)
            usericon = session['curr_user']['usericon']
            # flash('change accept!')
        if  request.form.get("phone_number"):
            phone = request.form.get("phone_number")
            mongo.db.users.update({'email': email},
                                  {"$set": {"phone": phone}})
            # flash('change accept!')
        if request.form.get("gender"):
            gender = request.form.get("gender")
            mongo.db.users.update({'email': email},
                                  {"$set": {"gender": gender}})
            # flash('change accept!')

        if request.form.get("birth_date"):
            birth = request.form.get("birth_date")
            mongo.db.users.update({'email': email},
                                  {"$set": {"birth": birth}})
            # flash('change accept!')
        if request.form.get("user_describe"):
            description = request.form.get("user_describe")
            mongo.db.users.update({'email': email},
                             {"$set": {"description":description}})
            # flash('change accept!')



    return render_template(userPage, username=username, usericon=usericon)


@app.route('/setting', methods=['GET', 'POST'])
def setting():
    if not session.get('username'):
        return redirect(url_for('login'))
    else:
        username = session['username']
        usericon = session['curr_user']['usericon']
        email = session['email']

    if request.method == 'POST':

        curr_password = request.form.get("curr_pw")
        new_password = request.form.get("new_pw")

        res = mongo.db.users.find({'email': email})
        password = list(res)[0]['password']
        if check_password_hash(password, curr_password):
            mongo.db.users.update({'email': email}, {"$set": {"password": generate_password_hash(new_password)}})
            flash('change password successful! ')
        else:
            flash('password is not correct!')
    return render_template(settingPage, username=username, usericon=usericon)


@app.route('/myorder', methods=['GET', 'POST'])
def myorder():
    if not session.get('username'):
        return redirect(url_for('login'))
    else:
        username = session['username']
        usericon = session['curr_user']['usericon']
        userid = session['userid']
        result = mongo.db.book.find({'user_id': userid}).sort([('time', 1)])
        mylist = list(result)
        # print(mylist)
        if result:
            myorder = json.loads(json_util.dumps(mylist))
            for i in myorder:
                picture = findpicture(i['property_id'])
                name = findname(i['property_id'])
                address = findaddress(i['property_id'])

                i.update({'picture': picture})
                i.update({'name': name})
                i.update({'address': address})
                if len(i['booked_time']) == 1:
                    i.update({'booked_time': i['booked_time'][0]})
                else:
                    i.update({'booked_time': i['booked_time'][0]+"-"+i['booked_time'][-1]})
        else:
            myorder = result
        # for i in order:
        #         order_detial=mongo.db.post.find({'_id': ObjectId(i['property_id'])})
        #         print('order_detial:',order_detial)
        #         order_list.append(order_detial)
        # else:
        #      order_list=result
        # print('myorder:', myorder)

    return render_template(myorderPage, username=username, usericon=usericon, order=myorder)


def findpicture(propertyid):
    result = mongo.db.post.find_one({'_id': ObjectId(propertyid)})
    picture = str(result['picture'][0])
    return picture


def findname(propertyid):
    result = mongo.db.post.find_one({'_id': ObjectId(propertyid)})
    name = str(result['name'])
    # print('name', name)
    return name


def findaddress(propertyid):
    result = mongo.db.post.find_one({'_id': ObjectId(propertyid)})
    address = str(result['address'])
    # print('ad', address)
    return address


def findbookdate(property_id):
    book_time = []
    result = mongo.db.book.find({'property_id': property_id})
    orderlist = list(result)
    if result:
        for i in orderlist:
            if len(i['booked_time']) == 1:
                book_time.append(i['booked_time'])
            else:
                book_time.append((i['booked_time'][0] + "-" + i['booked_time'][-1]))
    return book_time


@app.route('/mypro', methods=['GET', 'POST'])
def myproperty():
    if not session.get('username'):
        return redirect(url_for('login'))
    else:
        username = session['username']
        usericon = session['curr_user']['usericon']
        userid = session['userid']
        result = mongo.db.post.find({'userid': userid})
        mylist = list(result)
        # print(mylist)
        if result:
            myproperty = json.loads(json_util.dumps(mylist))
            for i in myproperty:
                i.update({'book_time': ''})
                property_id = i['_id']['$oid']
                book_time = findbookdate(property_id)
                if book_time:
                    i.update({'book_time': str(book_time)})
        else:
            myproperty = result

    return render_template(mypropertyPage, username=username, usericon=usericon, myproperty=myproperty)


def changeusername():
    if session.get('username'):  # if user is login and can get username
        email = session['email']  # find user in db though session email
        new_username = request.form.get("username")
        mongo.db.users.update({'email': email}, {"$set": {"username": new_username}})
        flash('change username successful!')
    else:
        flash('Please login first!')


#############verification code################
@app.route('/verification_code')
def serve_img():
    img, content = get_code()
    # print("content11111: ", content)
    # print("__________")
    session["real_verification_code"] = content
    # print(session["real_verification_code"], "session")

    basepath = app.config['basedir'] + '/static/images/'
    path = os.path.join(basepath, 'verification_code.jpg')
    img.save(path)
    # print("save path")

    tempFileObj = NamedTemporaryFile(mode='w+b', suffix='jpg')
    pilImage = open(path, 'rb')
    copyfileobj(pilImage, tempFileObj)
    pilImage.close()
    remove(path)
    tempFileObj.seek(0, 0)

    response = send_file(tempFileObj, as_attachment=True, attachment_filename='verification_code.jpg')

    return response


def serve_pil_image(pil_img):
    img_io = StringIO()
    pil_img.save(img_io, 'JPEG', quality=70)
    img_io.seek(0)
    return send_file(img_io, mimetype='image/jpeg')


def get_code(width=100, height=40, fontSize=25):

    img = Image.new("RGB", (width, height), getColor())
    draw = ImageDraw.Draw(img)
    path = app.config['basedir'] + '/static/fonts/OpenSans-Regular.ttf'
    # print("path1111111:", path)
    font = ImageFont.truetype(font=path, size=fontSize)
    # print("font:", font)

    content = myrandom()
    # print("content:", content)

    draw.text((width * 0.1, height * 0.15), content,
               colour=getColor(), font=font)

    for i in range(5):
        x = random.randint(0, 20)
        y = random.randint(0, height)
        z = random.randint(width - 20, width)
        w = random.randint(0, height)
        draw.line(((x, y), (z, w)), fill=getColor())

    # print("img:", img)
    # print("content____:", content)
    return img, content


def getColor():

    color = (random.randint(0, 256), random.randint(0, 256), random.randint(0, 256))
    # print("color:", color)
    return color


def myrandom(count=5):
    myList = list(string.ascii_letters + string.digits)
    # print("mylist first", myList[0])

    lists = random.sample(myList, count)
    # print("mylist", lists)

    return "".join(lists)


def get_Font(split='.ttf'):

    path = app.config['basedir'] + '/static/fonts/OpenSans-Regular.ttf'

    listFont = os.listdir(path)

    fontList = [path + x for x in listFont if os.path.splitext(x)[1] == split]

    path = random.sample(fontList, 1)
    # print("path:", path)


#####################################################################################################
###########check email#####################
from itsdangerous import URLSafeTimedSerializer


def generate_confirmation_token(email):
    serializer = URLSafeTimedSerializer(app.config['SECRET_KEY'])
    return serializer.dumps(email, salt=app.config['SECURITY_PASSWORD_SALT'])


def confirm_token(token, expiration=3600):
    serializer = URLSafeTimedSerializer(app.config['SECRET_KEY'])
    try:
        email = serializer.loads(
            token,
            salt=app.config['SECURITY_PASSWORD_SALT'],
            max_age=expiration
        )
    except:
        return False
    return email


@app.route('/confirm/<token>')
def confirm_email(token):
    try:
        email = confirm_token(token)
    except:
        flash('The confirmation link is invalid or has expired.', 'danger')
    # user = User.query.filter_by(email=email).first_or_404()
    result = mongo.db.users.find({'email': email})
    user = list(result)[0]
    if user['confirmed']:
        flash('Account already confirmed. Please login.', 'success')
    else:
        mongo.db.users.update({'email': email}, {"$set": {"confirmed": True, "comfirmedtime": datetime.now()}})
        # db.session.add(user)
        # db.session.commit()
        # print("okkkkkkkkkkkkk")
        flash('You have confirmed your account. Thanks!', 'success')
    return redirect(url_for('register3'))



@app.route('/resend')
def resend_confirmation():
    email = session['email']
    token = generate_confirmation_token(email)
    confirm_url = url_for('confirm_email', token=token, _external=True)
    html = render_template('activate.html', confirm_url=confirm_url)
    subject = "Please confirm your email"
    msg_typ = "html"
    common.sendEmail(email, subject, msg_typ, html)
    flash('A new confirmation email has been sent.', 'success')
    return redirect(url_for('register2'))

@app.route('/forget/<token>')
def reset_pw(token):
    return redirect(url_for('resetpassword'))



@app.route('/forget',methods=['GET', 'POST'])
def forgetpassword():
    if request.method == 'POST':
        email = request.form.get('email')
        session['email'] = email
        if not  mongo.db.users.find({'email': email}):
            flash('Not a member !')
        else:
            token = generate_confirmation_token(email)
            resetpw_url = url_for('reset_pw', token=token, _external=True)
            html = render_template('repassword.html', resetpw_url=resetpw_url)
            subject = "Reset password"
            msg_typ = "html"
            common.sendEmail(email, subject, msg_typ, html)
            return redirect(url_for('forget2'))
    return render_template("forget.html")

@app.route('/forget2', methods=['GET', 'POST'])
def forget2():
    return render_template("forget2.html")

@app.route('/resend2')
def resend_password():
    email = session['email']
    token = generate_confirmation_token(email)
    resetpw_url = url_for('reset_pw', token=token, _external=True)
    html = render_template('repassword.html', resetpw_url=resetpw_url)
    subject = "Reset password"
    msg_typ = "html"
    common.sendEmail(email, subject, msg_typ, html)
    return redirect(url_for('forget2'))

####reset password(forget password)#####

@app.route('/resetpw', methods=['GET', 'POST'])
def resetpassword():
    email = session['email']
    if request.method == 'POST':
        password1 = request.form.get("new_pw")
        password2 = request.form.get("con_pw")
        if password1 ==password2:
            new_password =password1
            mongo.db.users.update({'email': email},
                                  {"$set": {"password":generate_password_hash(new_password)}})
            session.pop('email', None)
            return redirect("login")
        else:
            flash('Two input should be same!')
    return render_template("resetpw.html")
