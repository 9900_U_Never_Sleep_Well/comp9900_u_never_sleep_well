    $(document).on('ready', function() {
      $(".variable").slick({
        dots: true,
        infinite: true,
        variableWidth: true
      });

      var diadate= eval($("#booked").val());
      var end_date= eval($("#end_date").val());
      //alert(end_date);
      $('#chstart_time').pickadate({
        format: 'yyyy/mm/dd',
        formatSubmit: 'yyyy/mm/dd',
        min: new Date(),
        max:end_date,
        disable:diadate,
      });

       $('#chend_time').pickadate({
        format: 'yyyy/mm/dd',
        formatSubmit: 'yyyy/mm/dd',
        min: new Date(),
        max:end_date,
        disable: diadate,
      });


    });