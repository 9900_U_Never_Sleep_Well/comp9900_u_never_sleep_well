$(document).ready(function(){
        if($('#searchtoggle').prop('checked')){
	        $('#searchshowbox').show();
	    }else{
	        $('#searchshowbox').hide();
	    }
	 $("#sortbuttons").hide();
	 $('#searchNotification').hide();
	 initalLikeButton();
    var date_input=$('input[name="indate"]'); //our date input has the name "date"
    var date_input2=$('input[name="outdate"]');
    date_input.datetimepicker({ format: 'Y/m/d', timepicker: false,lang:'en' });
    date_input2.datetimepicker({ format: 'Y/m/d', timepicker: false ,lang:'en'});
    var searchResult;
    var likeList;
    $.likeMethod.getLikeList();
    $('#indexSearchButton').click(function(){
        var form = $('#indexSearch');
	    var url = form.attr('action');
	    $.ajax({
	        type:'POST',
	        url:url,
	        data:form.serialize(),
	        success:function(data){}
	    });
    });
    var flag = $('#flag').attr("data-value");
    console.log(flag);
    if(flag == 1){
        $("#searchNotification").html("Searching")
        $('#searchNotification').show();
        var form = $('#searchForm');
	    var url = form.attr('action');
	    $.ajax({
	        type:'POST',
	        url:url,
	        data:form.serialize(),
	        success:function(data){
	            $('#flag').val("");
	            $('#flag').attr("data-value","0");
	            searchResult = data.data;
	            $("#sortbuttons").show();
	            if(searchResult.length<2){
	                $("#totalresults").html("Total "+searchResult.length+" result");
	            }else{
	                $("#totalresults").html("Total "+searchResult.length+" results");
	            }
	            $.pageMethod.getPage($.sortMethods.ascendingItemList(searchResult));
                $('#searchNotification').hide(300);
	        }
	    });
    }
	$('#searchButton').click(function(e){
	    $("#searchNotification").html("Searching")
	    $('#searchNotification').show();
	    var form = $('#searchForm');
	    var url = form.attr('action');
	    $.ajax({
	        type:'POST',
	        url:url,
	        data:form.serialize(),
	        success:function(data){
	            searchResult = data.data;
	            console.log(searchResult);
	            $("#sortbuttons").show();
	            if(searchResult.length<2){
	                $("#totalresults").html("Total "+searchResult.length+" result");
	            }else{
	                $("#totalresults").html("Total "+searchResult.length+" results");
	            }
	            $.pageMethod.getPage($.sortMethods.ascendingItemList(searchResult));
                $('#searchNotification').hide(300);
	        }
	    });
	})
	$('#low').click(function(){
	    var results = $.sortMethods.ascendingItemList(searchResult);
	    searchResult = results;
	    $.pageMethod.getPage(results);
	});
	$('#high').click(function(){
	    var results = $.sortMethods.decendingItemList(searchResult);
	    searchResult = results;
	    $.pageMethod.getPage(results);
	});
	$('#searchtoggle').change(function(){
	    if($(this).prop('checked')){
	        $('#searchshowbox').show(1000);
	    }else{
	        $('#searchshowbox').hide(1000);
	    }
	})
	$('#compareButton').on('click',function(){
        $('.hdr-srch').addClass('active');
        return false;
    });
});
$.likeMethod = {
    addLikeRecord:function(id){
        $.post("/addLikeRecord",{"postid":id},function(data){
            likeList = data.data;
            if(typeof(likeList) == 'undefined'){
                window.location.replace("/login");
            }
            var strhtml = $.doProducts.showLikeArray(likeList);
            var strhtml2 = $.doProducts.showLikeListArray(likeList);
            $('#likeList').html(strhtml);
            $('#compareArea').html(strhtml2);
            initalLikeList();
            $('#likelistlen').html(likeList.length);
        })
    },
    getLikeList:function(){
        $.post("/getLikeList",{},function(data){
            likeList = data.data;
            var strhtml = $.doProducts.showLikeArray(likeList);
            var strhtml2 = $.doProducts.showLikeListArray(likeList);
            $('#likeList').html(strhtml);
            $('#compareArea').html(strhtml2);
            initalLikeList();
            $('#likelistlen').html(likeList.length);
        })
    },
    delLikeRecord:function(recordid){
        $.post("/delLikeRecord",{"recordid":recordid},function(data){
            likeList = data.data;
            if(typeof(likeList) == 'undefined'){
                window.location.replace("/login");
            }
            var strhtml = $.doProducts.showLikeArray(likeList);
            var strhtml2 = $.doProducts.showLikeListArray(likeList);
            $('#likeList').html(strhtml);
            $('#compareArea').html(strhtml2);
            initalLikeList();
            $('#likelistlen').html(likeList.length);
            $("#searchNotification").html("Delete successfully");
            $('#searchNotification').show();
            setTimeout(function(){
                $('#searchNotification').hide(300);
            },1500);
        })
    }
};
$.doProducts = {
	showItemArray:function(arr){
		var strhtml = ""
		var newTitle = ""
		for(var i=0;i<arr.length;i++){
		    if(arr[i].name.length>30){
		        newTitle = arr[i].name.slice(0,30)
		        newTitle +="...";
		    }else{
		        newTitle = arr[i].name
		    }
			strhtml += "<div class='col-md-6 col-sm-6 col-lg-6'><div class='cas-bx'>";
			strhtml += "<a href='/post_detail/"+arr[i]._id+"' target='_blank'><img src='"+arr[i].picture[0]+"'></a>";
			strhtml += "<div class='cas-inf'><h4 itemprop='headline'>";
			strhtml += "<a href='/post_detail/"+arr[i]._id+"' target='_blank'>"+newTitle+"</a>";
			strhtml += "<div class='pst-btm'><div class='pst-tgs'><i class='fa fa-map-marker'></i>";
			strhtml += "<a>"+arr[i].address+"</a></div>";
			strhtml += "<div class='lk-cmt'><a> $"+arr[i].price+" </a>";
			strhtml += "<i class='fa fa-heart searchLike' data-id='"+arr[i]._id;
			strhtml+="' data-title='"+arr[i].name+"' data-like='like'></i>"
			strhtml += "</div></div></div></div></div>"
		}

		return strhtml;
	},
	showLikeArray:function(arr){
	    var strhtml="";
	    var newTitle ="";
	    for(var i=0;i<arr.length;i++){
	        if(arr[i].name.length>15){
		        newTitle = arr[i].name.slice(0,13)
		        newTitle +="...";
		    }else{
		        newTitle = arr[i].name
		    }
	        strhtml += "<li><span> $"+arr[i].price+"</span> ";
	        strhtml += "<a href='/post_detail/"+arr[i]._id+"' itemprop='url' target='_blank' title='"+arr[i].name+"'>";
	        strhtml += newTitle+"</a>";
	        strhtml += "<i class='fa fa-close delLike' onclick=delLikeFunc('"+arr[i].recordid+"')></i></li>";
	    }
	    return strhtml;
	},
	showLikeListArray:function(arr){
	    var len = arr.length;
	    var sm = 0;
	    var strhtml = "<div class='' id='compareAreaOWL'>";
	    var newdes =""
	    if(len>4){
	        sm = len-4;
	    }
	    for(var i=len-1;i>=0;i--){
	        newdes = arr[i].describe
	        if(arr[i].describe.length>120){
		        newdes = arr[i].describe.slice(0,120)
		        newdes +="...";
		    }
		    strhtml += "<div class='pln-bx2'><div class='pnb-bdy2'>";
		    strhtml += "<img src='"+arr[i].picture[0]+"' class='mckp-tp'>";
		    strhtml += "<span>$"+arr[i].price+"</span>";
		    strhtml += "<h5>"+arr[i].name+"</h5>";
		    strhtml += "<p>"+arr[i].beds+" bed</p>";
		    strhtml += "<p>"+arr[i].bedrooms+" bedroom</p>";
		    if(arr[i].bathrooms==""){
		        strhtml += "<p>"+0+" bathroom</p>";
		    }else{
		        strhtml += "<p>"+arr[i].bathrooms+" bathroom</p>";
		    }
		    strhtml += "<p>"+arr[i].type+"</p>";
		    strhtml += "<p>"+arr[i].city+"</p>";
		    strhtml += "<p>"+newdes+"</p>";
		    strhtml +=  "<a ><i class='fa fa-close' onclick=delLikeFunc('"+arr[i].recordid+"')></i></a></div>";
		    strhtml +=  "<a href='/post_detail/"+arr[i]._id+"' target='_blank'>View More</a></div>";
	    }
	    strhtml +="</div>"
	    return strhtml;
	}
}


$.pageMethod ={
	getPage:function(pagedata){
		$('#page').pagination({
			dataSource: pagedata,
			pageSize: 8,
			autoHidePrevious: true,
			autoHideNext: true,
			afterPageOnClick:function(){
				$("body,html").animate({scrollTop:0},200);
			},
			callback: function(data, pagination) {
				var html = $.doProducts.showItemArray(data);
				$("#pageContain").html(html);
				initalLikeButton();
			}

		})
	}
}
$.sortMethods = {
	ascendingItemList:function(products){
		var results =[];
		products.sort(function(a,b){
		    var t1 = a.price.replace(",","");
		    var t2 = b.price.replace(",","");
			return t1-t2;
		});
		for(var i=0;i<products.length;i++){
			results.push(products[i]);
		}
		return results;
	},
	decendingItemList:function(products){
		var results =[];
		products.sort(function(a,b){
		    var t1 = a.price.replace(",","");
		    var t2 = b.price.replace(",","");
			return t1-t2;
		});
		for(var i=products.length-1;i>=0;i--){
			results.push(products[i]);
		}
		return results;
	}
}

function delLikeFunc(recordid){
    $.likeMethod.delLikeRecord(recordid);
}
function initalLikeButton(){
    $('.searchLike').click(function(){
                    var id = $(this).attr("data-id");
	                console.log(id);
                    if($(this).hasClass('searchLikeActive')){
                        $("#searchNotification").html("Has been added")
                        $('#searchNotification').show();
                        setTimeout(function(){
                            $('#searchNotification').hide(300);
                        },1000);
                    }else{
                        $.likeMethod.addLikeRecord(id);
                        $("#searchNotification").html("Add successfully")
                        $('#searchNotification').show();
                        setTimeout(function(){
                            $('#searchNotification').hide(300);
                        },1000);
                    }
                });
}

function initalLikeList(){
    $('#compareAreaOWL').owlCarousel({
            autoplay: false,
            smartSpeed: 800,
            loop: false,
            items: 4,
            dots: true,
            slideSpeed: 2000,
            nav: true,
            margin: 20,
            responsive: {
                0: {items: 1},
                480: {items: 2},
                768: {items: 3},
                1200: {items: 4}
            },
            navText: [
                "<i class='fa fa-angle-left'></i>",
                "<i class='fa fa-angle-right'></i>"
            ]
        });
}