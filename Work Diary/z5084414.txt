week 2
I created Bitbucket and Trello account. Group formed. After discussion with our group , the project title and basic needs were determined, which prepared for the next week's project proposal.
week 3
attend the second group meeting, write partial project proposal.
week 4
I create database table for user. Start to learn using mongoDB as our project database.
week 5
The login page has done, connect with the database successfully.
week 6
The front-end page for the first step of registration is completed, including front-end validation of the user name and password setting.
week 7
Connect the information for the first page to the back-end database, Implement password encryption storage, check the uniqueness of email, and add the verification code.
week 8 
Complete the front page of the second and third steps of registration, and add the automatic time jump module in the third step page.
week 9
Complete the registration mailbox verification function.The login registration function is basically completed, and the user information module will be done next.
week 10
Start do userprofile part, finnish the front end, add user avatar feature, automatically generate new avatars to new users.  
week 11
 Finish the back end of userprofile part, there are four features, show user's property,user's order, change password and change profile infomation. This week also do the final debugging.